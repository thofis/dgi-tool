package com.gitlab.thofis.dgitool.model;

import org.junit.Test;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;
import static org.assertj.core.api.Assertions.assertThat;

public class CreationDateTest {

    @Test(expected = IllegalArgumentException.class)
    public void invalidMinDate() {
        CreationDate.of(1800, 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidDateInFuture() {
        LocalDate now = LocalDate.now();
        CreationDate.of(now.getYear(),
                now.getMonth()
                        .getValue(),
                now.plus(1, DAYS)
                        .getDayOfMonth());
    }

    @Test
    public void validDateToday() {
        LocalDate now = LocalDate.now();
        CreationDate creationDate = CreationDate.of(now.getYear(),
                now.getMonth()
                        .getValue(),
                now.getDayOfMonth());
        assertThat(creationDate.getValue()).isEqualTo(now);
    }
}
