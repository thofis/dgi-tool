package com.gitlab.thofis.dgitool.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NameTest {
    @Test(expected = NullPointerException.class)
    public void notEmpty() {
        Name.of(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tooLong() {
        Name.of("012345678901234567890123456789012345678901234567890123456789");
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidCharacters() {
        Name.of("Test .");
    }

    @Test
    public void validTitle() {
        Name aName = Name.of("James Bond");
        assertThat(aName.getValue())
                .isEqualTo("James Bond");
    }
}
