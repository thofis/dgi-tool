package com.gitlab.thofis.dgitool.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TitleTest {
    @Test(expected = NullPointerException.class)
    public void notEmpty() {
        Title.of(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tooLong() {
        Title.of("012345678901234567890123456789");
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidCharacters() {
        Title.of("Test .");
    }

    @Test
    public void validTitle() {
        Title title = Title.of("Title");
        assertThat(title.getValue())
                .isEqualTo("Title");
    }
}
