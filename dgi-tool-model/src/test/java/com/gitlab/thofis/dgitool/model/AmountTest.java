package com.gitlab.thofis.dgitool.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AmountTest {
    @Test(expected = IllegalArgumentException.class)
    public void toLow() {
        Amount.of(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void toHigh() {
        Amount.of(Integer.MAX_VALUE);
    }

    @Test
    public void validAmount() {
        Amount title = Amount.of(10);
        assertThat(title.getValue())
                .isEqualTo(10);
    }
}
