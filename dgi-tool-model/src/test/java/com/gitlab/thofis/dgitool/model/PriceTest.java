package com.gitlab.thofis.dgitool.model;

import org.javamoney.moneta.Money;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class PriceTest {
    @Test(expected = NullPointerException.class)
    public void notEmptyAmount() {
        Price.of((Number) null);
    }

    @Test(expected = NullPointerException.class)
    public void notEmptyCurrency() {
        Price.of(1.0, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidNegative() {
        Price.of(-1.0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidCurrency() {
        Price.of(1.0, "GBP");
    }

    @Test
    public void validPriceInEUR() {
        Price price = Price.of(1.0);
        assertThat(price.getValue())
                .isEqualTo(Money.of(1.0, "EUR"));
    }


    @Test
    public void validPriceInUSD() {
        Price price = Price.of(1.0, "USD");
        assertThat(price.getValue())
                .isEqualTo(Money.of(1.0, "USD"));
    }
}
