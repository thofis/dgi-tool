package com.gitlab.thofis.dgitool.model;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TickerSymbolTest {
    @Test(expected = NullPointerException.class)
    public void notEmpty() {
        TickerSymbol.of(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void tooLong() {
        TickerSymbol.of("ABCDEFG");
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidCharacters() {
        TickerSymbol.of("Test");
    }

    @Test
    public void validTitle() {
        TickerSymbol title = TickerSymbol.of("MSFT");
        assertThat(title.getValue())
                .isEqualTo("MSFT");
    }

    @Test
    public void validTitleWithDot() {
        TickerSymbol title = TickerSymbol.of("BRK.A");
        assertThat(title.getValue())
                .isEqualTo("BRK.A");
    }
}
