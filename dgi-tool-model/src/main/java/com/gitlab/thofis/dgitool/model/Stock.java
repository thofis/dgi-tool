package com.gitlab.thofis.dgitool.model;

import lombok.Value;

@Value
public class Stock {
    private TickerSymbol tickerSymbol;
    private Title title;
}
