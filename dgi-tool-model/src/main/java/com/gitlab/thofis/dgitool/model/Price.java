package com.gitlab.thofis.dgitool.model;

import lombok.Delegate;
import lombok.Value;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;

import static com.gitlab.thofis.dgitool.model.ValidationUtil.validateMonetaryAmount;

@Value
public class Price {
    @Delegate
    MonetaryAmount value;

    private Price(Number amount, String currency) {
        validateMonetaryAmount(amount, currency);
        this.value = Money.of(amount, currency);
    }

    public static Price of(Number amount, String currency) {
        return new Price(amount, currency);
    }

    public static Price of(Number amount) {
        // currency default to EUR if not specified
        return new Price(amount, "EUR");
    }

    public static Price of(MonetaryAmount monetaryAmount) {
        return new Price(monetaryAmount.getNumber(), monetaryAmount.getCurrency()
                .getCurrencyCode());
    }


}
