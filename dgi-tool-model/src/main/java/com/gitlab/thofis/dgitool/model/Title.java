package com.gitlab.thofis.dgitool.model;

import lombok.Value;

import static com.gitlab.thofis.dgitool.model.ValidationUtil.validateStringValue;

//@Getter
@Value(staticConstructor = "of")
public class Title {
    private static final String TITLE_PATTERN = "[a-zA-Z0-9 ]{1,20}";
    private String value;

    private Title(String value) {
        validateStringValue(value, TITLE_PATTERN);
        this.value = value;
    }
}
