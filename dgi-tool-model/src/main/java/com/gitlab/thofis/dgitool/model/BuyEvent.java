package com.gitlab.thofis.dgitool.model;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
public class BuyEvent extends HoldingEvent {
    Amount amount;
    Price price;
    Price tradingCentreFee;
    Price tradingFee;

    @Override
    protected void applyOnAmount(Holding holding) {
        holding.setAmount(holding.amount.add(amount));
    }

    protected Price cost() {
        return Price.of(price.add(tradingCentreFee.getValue())
                .add(tradingFee.getValue()));
    }
}
