package com.gitlab.thofis.dgitool.model;

import lombok.Data;

import javax.money.MonetaryAmount;
import java.util.Set;

@Data
public class Portfolio {
    private Title title;
    private Name shareholder;
    private CreationDate creationDate;
    private Set<Holding> holdings;
    private MonetaryAmount cash;
}
