package com.gitlab.thofis.dgitool.model;

import lombok.Value;

import static com.gitlab.thofis.dgitool.model.ValidationUtil.validateStringValue;

@Value(staticConstructor = "of")
public class Name {
    private static final String NAME_PATTERN = "[a-zA-Z0-9 ]{3,40}";
    private String value;

    private Name(String value) {
        validateStringValue(value, NAME_PATTERN);
        this.value = value;
    }
}

