package com.gitlab.thofis.dgitool.model;

import lombok.Data;

import java.util.Set;

@Data
public class Holding {
    Amount amount;
    Stock stock;
    CreationDate creationDate;
    // Set of events for this holding sorted by creation Date
    Set<HoldingEvent> holdingEvents;
}
