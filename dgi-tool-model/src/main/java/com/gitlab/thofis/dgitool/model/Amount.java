package com.gitlab.thofis.dgitool.model;

import lombok.Value;

import static com.gitlab.thofis.dgitool.model.ValidationUtil.validateIntValue;

@Value(staticConstructor = "of")
public class Amount {
    private int value;

    private Amount(int value) {
        validateIntValue(value, 0, 1_000_000_000);
        this.value = value;
    }

    public Amount add(Amount other) {
        return Amount.of(value + other.getValue());
    }

    public Amount subtract(Amount other) {
        return Amount.of(value - other.getValue());
    }

    public Amount multiply(Amount factor) {
        return Amount.of(value * factor.getValue());
    }

    public double multiply(double exchangeRate) {
        return this.value * exchangeRate;
    }
}
