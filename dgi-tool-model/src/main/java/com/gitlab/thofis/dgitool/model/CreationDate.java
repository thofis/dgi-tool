package com.gitlab.thofis.dgitool.model;

import lombok.Value;

import java.time.LocalDate;

@Value
public class CreationDate {
    private LocalDate value;

    private CreationDate(int year, int month, int day) {
        // LocalDate will make sure itself, that parameters are valid;
        this.value = LocalDate.of(year, month, day);
        LocalDate minDate = LocalDate.of(1900, 1, 1);
        // creation date not allowed to be in the future
        LocalDate maxDate = LocalDate.now();
        ValidationUtil.validateDateValue(value, minDate, maxDate);
    }

    public static CreationDate of(int year, int month, int day) {
        return new CreationDate(year, month, day);
    }

    private CreationDate(LocalDate localDate) {
        // avoid that lombok creates a public constructor for this
        throw new UnsupportedOperationException();
    }
}
