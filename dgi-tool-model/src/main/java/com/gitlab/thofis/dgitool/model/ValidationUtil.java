package com.gitlab.thofis.dgitool.model;

import java.time.LocalDate;

import static org.apache.commons.lang3.Validate.matchesPattern;
import static org.apache.commons.lang3.Validate.notNull;

/**
 * Collection of methods for validating.
 */
public final class ValidationUtil {

    private ValidationUtil() {
        throw new UnsupportedOperationException();
    }

    public static void validateStringValue(String value, String pattern) {
        notNull(value);
        matchesPattern(value, pattern);
    }


    public static void validateDateValue(LocalDate value, LocalDate minDate, LocalDate maxDate) {
        if (value.compareTo(minDate) < 0) {
            throw new IllegalArgumentException("date not allowed to be prior to " + minDate.toString());
        }
        if (value.compareTo(maxDate) > 0) {
            throw new IllegalArgumentException("date not allowed to be after " + maxDate.toString());
        }
    }

    public static void validateIntValue(int value, int minValue, int maxValue) {
        if (value < minValue || value > maxValue) {
            final String message = String.format("value has to be greater than %d and less thean %d", minValue, maxValue);
            throw new IllegalArgumentException(message);
        }
    }

    public static void validateMonetaryAmount(Number amount, String currency) {
        if (currency == null) throw new NullPointerException("currency must not be empty");
        if (amount == null) throw new NullPointerException("amount must not be null");
        if (amount.doubleValue() < 0) {
            throw new IllegalArgumentException("price has to be a non negative value");
        }
        if (!currency.equals("EUR") && !currency.equals("USD")) {
            throw new IllegalArgumentException("currency has to be 'EUR' or 'USD'");
        }
    }
}
