package com.gitlab.thofis.dgitool.model;

public abstract class HoldingEvent {
    CreationDate creationDate;

    protected abstract void applyOnAmount(Holding holding);
}

