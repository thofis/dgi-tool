package com.gitlab.thofis.dgitool.model;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
public class DividendPayoutEvent extends HoldingEvent {
    double dividendRate;
    double exchangeRate;

    @Override
    protected void applyOnAmount(Holding holding) {
        // nothing to do here in this case
    }

    protected Price earnings(Holding holding) {
        Amount holdingAmount = holding.getAmount();
        double nativeDividendPayout = holdingAmount.multiply(exchangeRate);
        double payoutAfterCurrencyConversion = nativeDividendPayout * (1 / exchangeRate);
        return Price.of(payoutAfterCurrencyConversion);
    }
}
