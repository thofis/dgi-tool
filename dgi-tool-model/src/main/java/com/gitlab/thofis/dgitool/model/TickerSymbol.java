package com.gitlab.thofis.dgitool.model;

import lombok.Value;

import static com.gitlab.thofis.dgitool.model.ValidationUtil.validateStringValue;

@Value(staticConstructor = "of")
public class TickerSymbol {
    public static final String TICKER_SYMBOL_PATTERN = "[A-Z.]{1,5}";
    private String value;

    private TickerSymbol(String value) {
        validateStringValue(value, TICKER_SYMBOL_PATTERN);
        this.value = value;
    }
}
