package com.gitlab.thofis.dgitool.model;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
public class SplitEvent extends HoldingEvent {
    Amount factor;

    @Override
    protected void applyOnAmount(Holding holding) {
        holding.setAmount(holding.getAmount()
                .multiply(factor));
    }
}
