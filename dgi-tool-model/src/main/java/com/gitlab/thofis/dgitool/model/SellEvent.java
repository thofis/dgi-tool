package com.gitlab.thofis.dgitool.model;

import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@EqualsAndHashCode(callSuper = true)
public class SellEvent extends HoldingEvent {
    Amount amount;
    Price price;
    Price tradingCentreFee;
    Price tradingFee;

    @Override
    protected void applyOnAmount(Holding holding) {
        holding.setAmount(holding.amount.subtract(amount));
    }

    protected Price earnings() {
        return Price.of(price.subtract(tradingCentreFee.getValue())
                .subtract(tradingFee.getValue()));
    }
}
